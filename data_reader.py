import pandas as pd
from Bio import SeqIO
import preprocess as ppr
import configs
import compatibility
from os import path
import pickle

def read_methylations(address, context, coverage_threshold = 10):
    methylations = pd.read_table(address)
    methylations.columns = ['chr', 'position', 'strand', 'meth', 'unmeth', 'context', 'three']
    methylations.drop(['three'], axis=1)
    if len(context) != 0:
        methylations = methylations[methylations['context'] == context]
    methylations = methylations[methylations['meth'] + methylations['unmeth'] > coverage_threshold]
    if len(context) != 0:
        methylations.drop(['context'], axis=1)
    methylations.drop(['strand'], axis=1)
    methylations = methylations.reset_index(drop=True)
    return methylations

def readfasta(address):
    recs = SeqIO.parse(address, "fasta")
    sequences = {}
    for chro in recs:
        sequences[chro.id] = chro.seq
    for i in sequences.keys():
        sequences[i] = sequences[i].upper()
    return sequences

def read_annot(address, chromosomes = None):
    annot_df = pd.read_table(address, sep='\t', comment='#')
    annot_df.columns = ['chr', 'source', 'type', 'start', 'end', 'score', 'strand', 'phase', 'attributes']
    if chromosomes != None:
        annot_chrs = annot_df.chr.unique()
        for chr in annot_chrs:
            if chr not in chromosomes:
                annot_df = annot_df[annot_df['chr'] != chr]
    return annot_df

def get_annotaions_df(cnfg):
    ad = ppr.get_riched_annot(cnfg['organism_name'])
    if cnfg['organism_name'] == configs.Cowpea_config['organism_name']:
        ad = compatibility.cowpea_annotation_compatibility(ad)
    if cnfg['organism_name'] == configs.Tomato_config['organism_name']:
        ad = compatibility.tomato_annotation_compatibility(ad, cnfg['chromosomes'])
    return ad

def get_seqeuences(cnfg):
    sequences = readfasta(cnfg['seq_address'])
    if cnfg['organism_name'] == configs.Cowpea_config['organism_name']:
        sequences = compatibility.cowpea_sequence_dic_key_compatibility(sequences)
    if cnfg['organism_name'] == configs.Tomato_config['organism_name']:
        sequences = compatibility.tomato_sequence_dic_key_compatibility(sequences, cnfg['chromosomes'])
    return sequences

def get_methylations(cnfg, coverage_threshold):
    methylations = read_methylations(cnfg['methylation_address'], '', coverage_threshold=coverage_threshold)
    if cnfg['organism_name'] == configs.Cowpea_config['organism_name']:
        methylations = compatibility.cowpea_methylation_compatibility(methylations)
    if cnfg['organism_name'] == configs.Tomato_config['organism_name']:
        methylations = compatibility.tomato_methylation_compatibility(methylations, cnfg['chromosomes'])
    return methylations

def get_data_batch(cnfg, coverage_threshold, allow_pickle=False):
    fn = './dump_files/' + cnfg['organism_name'] + '_'+ str(coverage_threshold) +'.pkl'
    if allow_pickle and path.exists(fn):
        pk = load_dic(fn)
        print('Data For ' + cnfg['organism_name'] + ' is loading from pickle')
        return pk['methylatoins'], pk['sequences'], pk['ad'], pk['meth_seq'], pk['cntxt_seq']
    ad = get_annotaions_df(cnfg)
    sequences = get_seqeuences(cnfg)
    methylations = get_methylations(cnfg, coverage_threshold)
    meth_seq = ppr.make_meth_string(cnfg['organism_name'], methylations, sequences, from_file=False)
    cntxt_seq = ppr.make_context_string(cnfg['organism_name'], methylations, sequences, from_file=False)
    if allow_pickle:
        save_dic(fn, {'methylatoins': methylations, 'sequences': sequences, 'ad': ad, 'meth_seq': meth_seq, 'cntxt_seq': cntxt_seq})
    return methylations, sequences, ad, meth_seq, cntxt_seq


def save_dic(file_name, dic):
    f = open(file_name, "wb")
    pickle.dump(dic, f)
    f.close()

def load_dic(file_name):
    f = open(file_name, "rb")
    res = pickle.load(f)
    f.close()
    return res

def save_gff(df, fn):
    df.to_csv(fn, header=False, index=False, sep='\t')

import preprocess as ppr
import process as pr
import pandas as pd
import clustering as clst
import random
import numpy as np


def gene_repeat_corr(cnfg1, cnfg2, rc):
    ad1 = ppr.get_riched_annot(cnfg1['organism_name'])
    ad2 = ppr.get_riched_annot(cnfg2['organism_name'])
    hml_df = ppr.make_homoloug_gene_df(cnfg1, cnfg2)
    ad1 = ad1[ad1.type == 'gene']
    ad1['g_length1'] = ad1['end'] - ad1['start']
    ad2 = ad2[ad2.type == 'gene']
    ad2['g_length2'] = ad2['end'] - ad2['start']
    covered_arr1 = pr.get_covered_genes(cnfg1, rc)
    hml_df.columns = ['g_ids1', 'g_ids2']
    covered_arr1.columns = ['fu1', 'gb1', 'fd1', 'g_ids1']
    covered_arr2 = pr.get_covered_genes(cnfg2, rc)
    covered_arr2.columns = ['fu2', 'gb2', 'fd2', 'g_ids2']
    res_df = pd.merge(hml_df, covered_arr1, how='left', on=['g_ids1', 'g_ids1'])
    res_df = pd.merge(res_df, covered_arr2, how='left', on=['g_ids2', 'g_ids2'])
    ad1.rename(columns={'gene_id': 'g_ids1'}, inplace=True)
    res_df = pd.merge(res_df, ad1[['g_ids1', 'g_length1']], how='left', on=['g_ids1', 'g_ids1'])
    ad2.rename(columns={'gene_id': 'g_ids2'}, inplace=True)
    res_df = pd.merge(res_df, ad2[['g_ids2', 'g_length2']], how='left', on=['g_ids2', 'g_ids2'])
    res_df['fu1'] = res_df['fu1']/rc['flanking_size']
    res_df['fu2'] = res_df['fu2']/rc['flanking_size']
    res_df['fd1'] = res_df['fd1']/rc['flanking_size']
    res_df['fd2'] = res_df['fd2']/rc['flanking_size']
    res_df['gb1'] = res_df['gb1']/res_df['g_length1']
    res_df['gb2'] = res_df['gb2']/res_df['g_length2']
    res_df['diff_rep_coverage'] = res_df['fu1'] + res_df['gb1'] + res_df['fd1'] - (res_df['fu2'] + res_df['gb2'] + res_df['fd2'])
    return res_df

def make_general_df(cnfg1, cnfg2, rc):
    res_df = gene_repeat_corr(cnfg1, cnfg2, rc)
    meth_fgf_avg1, km_lbls1, g_ids1, clf = clst.train_clustering(cnfg1, rc)
    avg_df1 = ppr.get_weighted_average_fgf_meth(meth_fgf_avg1)
    avg_df1.columns = ['fu_m1', 'gb_m1', 'fd_m1']
    avg_df1['g_ids1'] = g_ids1
    avg_df1['clstr1'] = km_lbls1
    res_df = pd.merge(res_df, avg_df1, how='left', on=['g_ids1', 'g_ids1'])
    res_df = res_df.dropna()
    meth_fgf_avg2, km_lbls2, g_ids2 = clst.set_cluster(cnfg2, clf, rc)
    avg_df2 = ppr.get_weighted_average_fgf_meth(meth_fgf_avg2)
    avg_df2.columns = ['fu_m2', 'gb_m2', 'fd_m2']
    avg_df2['g_ids2'] = g_ids2
    avg_df2['clstr2'] = km_lbls2
    res_df = pd.merge(res_df, avg_df2, how='left', on=['g_ids2', 'g_ids2'])
    res_df = res_df.dropna()
    return res_df

def check_atleast_one(res_df):
    res_df = res_df.reset_index(drop=True)
    a = 0
    cnt = 0
    for i in range(2000):
        indx = random.randint(0, len(res_df))
        g_id = res_df.iloc[indx]['g_ids1']
        clstr = res_df.iloc[indx]['clstr1']
        sub = res_df[res_df.g_ids1 == g_id]
        a += len(sub)
        if clstr in np.asarray(sub['clstr2']):
            cnt += 1
    return cnt/2000, float(a)/2000

def check_existance_one_cluster(cnfg1, cnfg2, rc):
    res_df = make_general_df(cnfg1, cnfg2, rc)
    return check_atleast_one(res_df)

def check_random_one_cluster_existance(cnfg1, cnfg2, rc):
    res_df = make_general_df(cnfg1, cnfg2, rc)
    gc_dic = ppr.make_GeneID_to_clust_dic(pd.DataFrame({'g_ids': res_df['g_ids2'], 'clstr': res_df['clstr2']}))
    gc_df = ppr.make_df_from_dic(gc_dic)
    vc = gc_df['values'].value_counts()
    rndm_arr = np.asarray([])
    for key in vc.keys():
        rndm_arr = np.concatenate([rndm_arr, np.ones(vc[key])*key])
    np.random.shuffle(rndm_arr)
    gc_df['values'] = rndm_arr
    gc_df.columns = ['g_ids2', 'clstr2']
    cast_clstrs = pd.merge(pd.DataFrame(res_df['g_ids2']), gc_df, how='left', on=['g_ids2', 'g_ids2'])
    res_df['clstr2'] = cast_clstrs['clstr2']
    return check_atleast_one(res_df)




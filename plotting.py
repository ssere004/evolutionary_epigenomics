import matplotlib.pyplot as plt
import itertools

def gbm_plotting(Xt, labels, fn):
    plt.scatter(Xt.iloc[:,0], Xt.iloc[:,1], c=labels)
    plt.savefig(fn)
    plt.close()

def clustering_plt_pairwise(fn, X1, X2, labels1, labels2, og1, og2, context):
    fig = plt.figure()
    fig.set_figheight(5)
    fig.set_figwidth(15)
    fig.suptitle(og1 + ' ' + og2 + ' ' + context)
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    ax1.set_title(og1)
    ax2.set_title(og2)
    for i in range(len(X1)):
        ax1.plot(X1[i], label=labels1[i])
        ax1.legend()
    for i in range(len(X2)):
        ax2.plot(X2[i], label=labels2[i])
        ax2.legend()
    plt.savefig(fn)
    plt.close()


def clustering_plt(fn, X, labels, og, context):
    fig = plt.figure()
    fig.set_figheight(8)
    fig.set_figwidth(8)
    fig.suptitle(og + context)
    for i in range(len(X)):
        plt.plot(X[i], label=labels[i])
        plt.legend()
    plt.savefig(fn)
    plt.close()


def clustering_plt_panel(fn, Xs, Ls, og_names, title, nrow, ncol):
    fig, axs = plt.subplots(nrow, ncol)
    fig.set_figheight(nrow*3 + 2)
    fig.set_figwidth(ncol*4 + 2)
    fig.suptitle(title)
    for X, l, og, i in zip(Xs, Ls, og_names, range(len(Xs))):
        ax = axs[int(i / ncol), i % ncol]
        for j in range(len(X)):
            ax.plot(X[j], label=l[j])
        ax.legend()
        ax.set_title(og)
    plt.savefig(fn)
    plt.close()




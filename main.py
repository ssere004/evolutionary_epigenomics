

annot_df1 = data_reader.read_annot(configs.Arabidopsis_config['annot_address'])
annot_df2 = data_reader.read_annot(configs.Rice_config['annot_address'])
annot_df1 = annot_ID_extractor(organism_name, annot_df1)
annot_df2 = annot_ID_extractor(organism_name, annot_df2)

cmn_genes = pd.read_table('~/Organisms/common_genes/Arabidopsis-Rice.txt', sep='\t', header=None)
cmn_genes[1] = cmn_genes[1].str.replace('S', 's')
cmn_genes[1] = cmn_genes[1].str.replace('T', 't')

ar_shared_genes = set(cmn_genes[0]).intersection(set(annot_df1['g_ID']))
ri_shared_genes = set(cmn_genes[1]).intersection(set(annot_df2['g_ID']))

sub_annot1= annot_df1[annot_df1['g_ID'].isin(ar_shared_genes)]
sub_annot2= annot_df2[annot_df2['g_ID'].isin(ri_shared_genes)]
sub_annot1 = sub_annot1[sub_annot1['type'] == 'gene']
sub_annot2 = sub_annot2[sub_annot2['type'] == 'mRNA']
diff_sub_annot1 = pd.concat([annot_df1[annot_df1['type'] == 'gene'], sub_annot1]).drop_duplicates(keep=False)
diff_sub_annot2 = pd.concat([annot_df2[annot_df2['type'] == 'mRNA'], sub_annot2]).drop_duplicates(keep=False)

sub_annot1 = sub_annot1.reset_index(drop = None)
sub_annot2 = sub_annot2.reset_index(drop = None)
diff_sub_annot1 = diff_sub_annot1.reset_index(drop = None)
diff_sub_annot2 = diff_sub_annot2.reset_index(drop = None)

sub_annot1
sub_annot2
diff_sub_annot1
diff_sub_annot2


def annot_ID_extractor(organism_name, annot_df):
    if organism_name == configs.Arabidopsis_config['organism_name']:
        tmp = annot_df['attributes'].str.split(';', expand=True)
        tmp = tmp[0].str.split(' ', expand=True)
        tmp = tmp[1].str.replace('"', '')
        annot_df['g_ID'] = tmp
    if organism_name == configs.Rice_config['organism_name']:
        tmp = annot_df['attributes'].str.split(';', expand=True)
        tmp = tmp[0].str.split('=', expand=True)
        annot_df['g_ID'] = tmp[1]

pr_cl = -1
for index, row in tmp.iterrows():
    for c in range(35):
        if 'protein_id' in str(row[c]):
            if pr_cl == -1:
                pr_cl = c
            if c != pr_cl:
                print(index, c, pr_cl)
                break
print(pr_cl)
ad1 = data_reader.read_annot(configs.Arabidopsis_config['annot_address'])
ad2 = data_reader.read_annot(configs.Cowpea_config['annot_address'])
ad3 = data_reader.read_annot('/home/csgrads/ssere004/Organisms/Rice/GCF_001433935.1_IRGSP-1.0_genomic.gff')
ad4 = data_reader.read_annot(configs.Cucumber_config['annot_address'])
ad5 = data_reader.read_annot(configs.Tomato_config['annot_address'])


ad1['protein_id'] = ad1['attributes'].str.extract(r' protein_id "([A-Za-z0-9_\./\\-]*)"', expand=False) #['CDS', 'start_codon', 'stop_codon']
ad2['protein_id'] = ad2['attributes'].str.extract(r'ID=([A-Za-z0-9]*)', expand=False) #['mRNA', 'five_prime_UTR', 'CDS', 'gene', 'three_prime_UTR']
ad3['protein_id'] = ad3['attributes'].str.extract(r';protein_id=([A-Za-z0-9_\./\\-]*)', expand=False) #['CDS']
ad4['protein_id'] = ad4['attributes'].str.extract(r';protein_id=([A-Za-z0-9_\./\\-]*)', expand=False) #['CDS']
ad5['protein_id'] = ad5['attributes'].str.extract(r';protein_id=([A-Za-z0-9_\./\\-]*)', expand=False)#['CDS']

ad1['gene_id'] = ad1['attributes'].str.extract(r'gene_id "([A-Za-z0-9_\./\\-]*)"', expand=False)
ad2['gene_id'] = ad2['attributes'].str.extract(r'ID=([A-Za-z0-9]*)', expand=False) #['mRNA', 'five_prime_UTR', 'CDS', 'gene', 'three_prime_UTR']
ad3['gene_id'] = ad3['attributes'].str.extract(r'GeneID:([A-Za-z0-9_\./\\-]*)', expand=False) #['CDS']
ad4['gene_id'] = ad4['attributes'].str.extract(r'GeneID:([A-Za-z0-9_\./\\-]*)', expand=False) #['CDS']
ad5['gene_id'] = ad5['attributes'].str.extract(r'GeneID:([A-Za-z0-9_\./\\-]*)', expand=False)#['CDS']


root = '/home/csgrads/ssere004/Organisms/common_genes/Orthologues/'

org1 = 'Ar'
org2 = 'Co'

fn = root+'Orthologues_'+org1 + '_prtm/'+org1+'_prtm__v__'+org2+'_prtm.tsv'
hml_g = pd.read_table(fn)

org1_genes_str = ''
org2_genes_str = ''
org1_list = hml_g[org1+'_prtm'].tolist()
org2_list = hml_g[org2+'_prtm'].tolist()
for itm in org1_list:
    org1_genes_str+=itm +', '
for itm in org2_list:
    org2_genes_str+=itm +', '
org1_list = org1_genes_str.split(', ')
org2_list = org2_genes_str.split(', ')


org1_shared_genes = set(org1_list).intersection(set(ad1['protein_id']))
org2_shared_genes = set(org2_list).intersection(set(ad2['protein_id']))

sub_annot1 = ad1[ad1['protein_id'].isin(org1_shared_genes)]
sub_annot2 = ad2[ad2['protein_id'].isin(org2_shared_genes)]
sub_annot1 = sub_annot1[sub_annot1['type'] == 'gene']
sub_annot2 = sub_annot2[sub_annot2['type'] == 'mRNA']
diff_sub_annot1 = pd.concat([annot_df1[annot_df1['type'] == 'gene'], sub_annot1]).drop_duplicates(keep=False)
diff_sub_annot2 = pd.concat([annot_df2[annot_df2['type'] == 'mRNA'], sub_annot2]).drop_duplicates(keep=False)


import preprocess as pp
import data_reader as dr
import configs
import compatibility
cnfg1 = configs.Arabidopsis_config
cnfg2 = configs.Cowpea_config

coverage_threshold = 10

methylations1 = dr.read_methylations(cnfg1['methylation_address'], '', coverage_threshold=coverage_threshold)
methylations2 = dr.read_methylations(cnfg2['methylation_address'], '', coverage_threshold=coverage_threshold)
sequences1 = dr.readfasta(cnfg1['seq_address'])
sequences2 = dr.readfasta(cnfg2['seq_address'])

if cnfg1['organism_name'] == configs.Cowpea_config['organism_name']:
        sequences1 = compatibility.cowpea_sequence_dic_key_compatibility(sequences1)
        methylations1 = compatibility.cowpea_methylation_compatibility(methylations1)
        ad1 = compatibility.cowpea_annotation_compatibility(ad1)
if cnfg2['organism_name'] == configs.Cowpea_config['organism_name']:
        sequences2 = compatibility.cowpea_sequence_dic_key_compatibility(sequences2)
        methylations2 = compatibility.cowpea_methylation_compatibility(methylations2)
        ad2 = compatibility.cowpea_annotation_compatibility(ad2)

meth_seq1 = make_meth_string(cnfg1['organism_name'], methylations1, sequences1, from_file=False)
meth_seq2 = make_meth_string(cnfg2['organism_name'], methylations2, sequences2, from_file=False)


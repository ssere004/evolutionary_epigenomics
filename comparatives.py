import preprocess as ppr
import process as pr
import configs
import data_reader as dr
import numpy as np
import pandas as pd
import plotting as pl
import clustering as clst

#Make a dictionary shows the cluster as a function of gene_id
def make_gene_cluster_dic(df):
    res_dic = {}
    for index, row in df.iterrows():
        res_dic[row['g_ids']] = row['cluster_id']
    return res_dic

#Gets the homologous gene_ids and the cluster Ids as a function of gene_ids,then add the cluster rows to the dataframe,
def make_cluster_cols(mrg_df, res_dic1, res_dic2):
    mrg_df = mrg_df.reset_index(drop=True)
    clusts1 = np.zeros(len(mrg_df))
    clusts2 = np.zeros(len(mrg_df))
    for index, row in mrg_df.iterrows():
        try:
            clusts1[index] = res_dic1[row['gene1']]
        except:
            clusts1[index] = np.nan
        try:
            clusts2[index] = res_dic2[row['gene2']]
        except:
            clusts2[index] = np.nan
    mrg_df['clust1'] = clusts1
    mrg_df['clust2'] = clusts2
    return mrg_df

def make_general_hml_clstr_df(cnfgs, rc):
    ref_cnfg = cnfgs[0]
    meth_fgf_avg, km_lbls, g_ids, clf = clst.train_clustering(ref_cnfg, rc)
    res_df = pd.DataFrame({'g_ids_ref':    g_ids, 'clstr_ref': km_lbls})
    for cnfg in cnfgs[1:]:
        og = cnfg['organism_name'][:2]
        hml_df = ppr.make_homoloug_gene_df(ref_cnfg, cnfg)
        res_df.columns = ['g_ids_ref', 'g_ids_' + og]
        meth_fgf_avg, km_lbls, g_ids = clst.set_cluster(ref_cnfg, clf, rc)
        df = pd.DataFrame({'g_ids_' + og: g_ids, 'clstr_'+og: km_lbls})
        df = pd.merge(hml_df, df, how='inner', on=['g_ids_' + og, 'g_ids_' + og])
        res_df = pd.merge(res_df, df, how='left', on=['g_ids_ref', 'g_ids_ref'])
    return res_df

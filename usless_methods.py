import numpy as np
import plotting as pl

def find_dist_rows(bin_meth_df, l):
    res = np.zeros(len(bin_meth_df))
    cnt = 0
    for col in bin_meth_df.columns:
        res += np.power(bin_meth_df[col] - l[cnt], 2)
        cnt += 1
    return np.sqrt(res)



def find_unique_cluster(cnfg_list, ct=5, bin_num=10, threshold=0.5, flanking_size=1000, context=None, allow_pickle=True):
    km_lables, km_centers, res_df, og_names, gid_serie = get_combined_clusters(cnfg_list, ct=ct, bin_num=bin_num, threshold=threshold, flanking_size=flanking_size, context=context, allow_pickle=allow_pickle)
    res_df['og'] = np.asarray(og_names)
    res_df['g_id'] = gid_serie
    for cnfg in cnfg_list:
        og = cnfg['organism_name']
        meth_fgf_avg = res_df[res_df['og'] == og[0:2]]
        km_lbls = meth_fgf_avg['cluster_id']
        meth_fgf_avg = meth_fgf_avg.loc[:, meth_fgf_avg.columns != 'g_ids']
        fn = cnfg['organism_name'][:2] + '_' + str(context) + '_clustering.png'
        avgs, labels = get_avgs_n_cnts(meth_fgf_avg, km_lbls)
        pl.clustering_plt(fn, avgs, labels, cnfg['organism_name'], str(context))



def get_combined_clusters(cnfg_list, ct=5, bin_num=10, threshold=0.5, flanking_size=1000, context=None, allow_pickle=True):
    fn = './dump_files/general_clustering_'+str(context)+'.pkl'
    if allow_pickle and path.exists(fn):
        pk = dr.load_dic(fn)
        return pk['km_lables'], pk['km_centers'], pk['res_df'], pk['og_names'], pk['gid_serie']
    res_df = pd.DataFrame()
    og_names = pd.Series(dtype='str')
    gid_serie = pd.Series(dtype='str')
    for cnfg in cnfg_list:
        methylations, sequences, ad, meth_seq, cntxt_seq = dr.get_data_batch(cnfg, ct, allow_pickle=True)
        flac_down_avg, genes_avg, flac_up_avg, g_ids = pr.get_binned_meth_lvls(ad[ad.type == 'gene'].reset_index(drop=True), meth_seq,  bin_num, threshold=threshold, flanking_size=flanking_size, context_seq=cntxt_seq, context=context)
        df_bin_meth = pr.clean_binned_avgs(flac_down_avg, genes_avg, flac_up_avg, g_ids)
        gid_serie = pd.concat([gid_serie, df_bin_meth['g_ids']])
        df_bin_meth = df_bin_meth.loc[:, df_bin_meth.columns != 'g_ids']
        df_bin_meth = df_bin_meth.loc[:, df_bin_meth.columns != 'cluster_id']
        df_bin_meth = ppr.normalize(df_bin_meth)
        og_names = pd.concat([og_names, pd.Series(cnfg['organism_name'][:2]).repeat(len(df_bin_meth))])
        res_df = pd.concat([res_df, df_bin_meth])
    km_lables, km_centers = pr.gbm_clustering(res_df, n_clusters=5)
    plt_fn = 'avg_' + str(context) + '_clustering.png'
    avgs, labels = get_avgs_n_cnts(res_df, km_lables)
    pl.clustering_plt(plt_fn, avgs, labels, 'AVG', str(context))
    dr.save_dic(fn, {'km_lables': km_lables, 'km_centers': km_centers, 'res_df': res_df, 'og_names': og_names, 'gid_serie': gid_serie})
    return km_lables, km_centers, res_df, og_names, gid_serie

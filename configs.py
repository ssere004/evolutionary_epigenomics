
root = '/home/csgrads/ssere004/Organisms/'


Arabidopsis_config = {
    'methylation_address': '/home/csgrads/ssere004/Organisms/Arabidopsis/SRR3171614_1_trimmed_bismark_bt2.CX_report.txt',
    'seq_address': '/home/csgrads/ssere004/Organisms/Arabidopsis/GCF_000001735.4_TAIR10.1_genomic.fa',
    'annot_address': '/home/csgrads/ssere004/Organisms/Arabidopsis/GCF_000001735.4_TAIR10.1_genomic.gtf',
    'organism_name': 'Arabidopsis',
    'repeat_address': '/home/csgrads/ssere004/Organisms/Arabidopsis/repeats/GCF_000001735.4_TAIR10.1_genomic.fa.out.gff',
    'annot_types': ['gene', 'exon', 'CDS'],
    'genome_size': 119668634
}

Cowpea_config = {
    'methylation_address': root + 'Cowpea/2010_1_bismark_bt2_pe.CX_report.txt',
    'seq_address': root + 'Cowpea/Cowpea_Genome_1.0.fasta',
    'annot_address': root + 'Cowpea/Vunguiculata_540_v1.2.gene.gff3',
    'organism_name': 'Cowpea',
    'repeat_address': '/home/csgrads/ssere004/Organisms/Cowpea/repeats/Cowpea_Genome_1.0.fasta.out.gff',
    'annot_types': ['gene', 'CDS'],
    'genome_size': 519435864
}

Rice_config = {
    'methylation_address': root + 'Rice/SRR618545_final_bismark_bt2.CX_report.txt',
    'seq_address': root + 'Rice/IRGSP-1.0_genome.fasta',
    'annot_address': root + 'Rice/transcripts_exon.gff',
    'organism_name': 'Rice',
    'repeat_address': '/home/csgrads/ssere004/Organisms/Rice/repeats/IRGSP-1.0_genome.fasta.out.gff',
    'annot_types': ['mRNA', 'exon'],
    'genome_size': 373245519
}

Tomato_config = {
    'methylation_address': root + 'Tomato/SRR503393_output_forward_paired_bismark_bt2_pe.CX_report.txt',
    'seq_address': root + 'Tomato/GCF_000188115.4_SL3.0_genomic.fa',
    'annot_address': root + 'Tomato/GCF_000188115.4_SL3.0_genomic.gff',
    'organism_name': 'Tomato',
    'repeat_address': '/home/csgrads/ssere004/Organisms/Tomato/repeats/GCF_000188115.4_SL3.0_genomic.fa.out.gff',
    'annot_types': ['gene', 'exon', 'CDS'],
    'chromosomes': ['NC_015438.3', 'NC_015439.3', 'NC_015440.3', 'NC_015441.3', 'NC_015442.3', 'NC_015443.3',
                    'NC_015444.3', 'NC_015445.3', 'NC_015446.3', 'NC_015447.3', 'NC_015448.3', 'NC_015449.3'],
    'genome_size': 226640966
}

Cucumber_config = {
    'methylation_address': root + 'Cucumber/SRR5430777_1_bismark_bt2_pe.CX_report.txt',
    'seq_address': root + 'Cucumber/GCF_000004075.3_Cucumber_9930_V3_genomic.fa',
    'annot_address': root + 'Cucumber/GCF_000004075.3_Cucumber_9930_V3_genomic.gff',
    'organism_name': 'Cucumber',
    'repeat_address': '/home/csgrads/ssere004/Organisms/Cucumber/repeats/GCF_000004075.3_Cucumber_9930_V3_genomic.fa.out.gff',
    'annot_types': ['gene', 'exon', 'CDS'],
    'genome_size': 828349174
}

run_config = {
    'coverage_threshold': 5,
    'bin_num': 10,
    'threshold': 0.5,
    'flanking_size': 1000,
    'context': None,
    'allow_pickle': True
}

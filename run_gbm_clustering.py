import process as pr
import configs
import plotting as pl
import numpy as np
import data_reader as dr

cnfg = configs.Arabidopsis_config
coverage_threshold = 10
bin_num = 10

methylations, sequences, ad, meth_seq, cntxt_seq = dr.get_data_batch(cnfg, coverage_threshold)
genes_df = ad[ad.type == 'gene'].reset_index(drop=True)

flac_down_avg, genes_avg, flac_up_avg, g_ids = pr.get_binned_meth_lvls(genes_df, meth_seq,  bin_num, threshold=0.5, flanking_size=2000, context_seq=None, context=None)
meth_fgf_avg = np.concatenate((flac_down_avg, genes_avg, flac_up_avg), axis=1)
labels, _ = pr.gbm_clustering(meth_fgf_avg)
pl.gbm_plotting(pr.PCA_(meth_fgf_avg), labels, 'clustering_fgf_' + cnfg['organism_name'] +'.png')

import clustering as clstr
import numpy as np
import configs as configs


config_list = [configs.Arabidopsis_config,
               configs.Cowpea_config,
               configs.Rice_config,
               configs.Cucumber_config,
               configs.Tomato_config,
               ]

context_list = [None, 'CG', 'CHG', 'CHH']


res = []

for cnfg in config_list:
    rres = [cnfg['organism_name']]
    for context in context_list:
        rres.append(clstr.get_nan_percentage(cnfg, ct=10, bin_num=10, threshold=0.5, flanking_size=2000, context=context))
    res.append(rres)

np.savetxt("temp_run1.csv", res, delimiter=", ", fmt='% s')


res = []

for cnfg in config_list:
    rres = [cnfg['organism_name']]
    for context in context_list:
        rres.append(clstr.get_nan_percentage(cnfg, ct=5, bin_num=10, threshold=0.5, flanking_size=2000, context=context))
    res.append(rres)

np.savetxt("temp_run2.csv", res, delimiter=", ", fmt='% s')

res = []

for cnfg in config_list:
    rres = [cnfg['organism_name']]
    for context in context_list:
        rres.append(clstr.get_nan_percentage(cnfg, ct=3, bin_num=10, threshold=0.5, flanking_size=2000, context=context))
    res.append(rres)

np.savetxt("temp_run3.csv", res, delimiter=", ", fmt='% s')






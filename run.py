import clustering as clst
import configs as configs
import process as pr
import data_reader as dr
import preprocess as ppr
import compatibility
import numpy as np
import repeat_analysis as rpa


cnfg_list = [configs.Arabidopsis_config,
             configs.Cowpea_config,
             configs.Rice_config,
             configs.Cucumber_config,
             configs.Tomato_config,
             ]

context_list = [None, 'CG', 'CHG', 'CHH']
rc = configs.run_config





#Run for checking the the existence of at least one cluster in the homologous set of genes.
# res = []
# pairs = []
# for cnfg1 in cnfg_list:
#     for cnfg2 in cnfg_list:
#         og1 = cnfg1['organism_name']
#         og2 = cnfg2['organism_name']
#         if og1 != og2 and (og1, og2) not in pairs:
#             ratio, avg_gn = rpa.check_existance_one_cluster(cnfg1, cnfg2, rc)
#             row = [cnfg1['organism_name'][:2], cnfg2['organism_name'][:2], ratio, avg_gn]
#             print(row)
#             pairs.append((og1, og2))
#             res.append(row)
# np.savetxt('res.csv', res, delimiter="\t", fmt="%s")


#Run for finding the percentage of genomes covered with repeats
# for cnfg in cnfg_list:
#     methylations, sequences, ad, meth_seq, cntxt_seq = dr.get_data_batch(cnfg, rc['coverage_threshold'], allow_pickle=True)
#     genes_df = ad[ad.type == 'gene']
#     repeats = dr.read_annot(cnfg['repeat_address'])
#     if cnfg['organism_name'] == configs.Cowpea_config['organism_name']:
#         repeats = compatibility.cowpea_annotation_compatibility(repeats)
#     if cnfg['organism_name'] == configs.Tomato_config['organism_name']:
#         repeats = compatibility.tomato_annotation_compatibility(repeats, configs.Tomato_config['chromosomes'])
#     repeat_seq = ppr.make_annotseq_string(cnfg['organism_name'], 'repeat', repeats, sequences, from_file=True, strand_spec=False)
#     res.append([cnfg['organism_name'], str(pr.get_coverage_pctg(repeat_seq))])
# np.savetxt('repeat_coverage_pctgs.csv', res, delimiter="\t", fmt="%s")


#Run for calculating the percentage of each gene region covered by the repeats.
# res = []
# for cnfg in cnfg_list:
#     r = [cnfg['organism_name']]
#     a = clst.get_average_gene_repeat_colocation(cnfg, rc)
#     for i in a:
#         r.append(str(i))
#     res.append(r)
# np.savetxt('repeat_gene_pctgs.csv', res, delimiter="\t", fmt="%s")



#Run for finding colocation of repeats and gene clusters.
# for cnfg in cnfg_list:
#     for cntx in context_list:
#         rc['context'] = cntx
#         res = clst.correlation_with_repeats(cnfg, rc)
#         res.to_csv(cnfg['organism_name'][:2]+'_'+str(cntx)+'_repeat_corr.csv', header=False, index=False)




#Run for comparing context specific methylation percentage in repeat and non-repeat regions.
# res = []
# for cnfg in cnfg_list:
#     ct, allow_pickle, context = rc['coverage_threshold'], rc['allow_pickle'], rc['context']
#     methylations, sequences, ad, meth_seq, cntxt_seq = dr.get_data_batch(cnfg, ct, allow_pickle=allow_pickle)
#     genes_df = ad[ad.type == 'gene']
#     from_file = True
#     repeats = dr.read_annot(cnfg['repeat_address'])
#     if cnfg['organism_name'] == configs.Cowpea_config['organism_name']:
#         repeats = compatibility.cowpea_annotation_compatibility(repeats)
#         from_file = False
#     repeat_seq = ppr.make_annotseq_string(cnfg['organism_name'], 'repeat', repeats, sequences, from_file=from_file, strand_spec=False)
#     row = []
#     for cntxt in context_list:
#         rc['context'] = cntxt
#         a, b = pr.get_averg_meth_seqbase(meth_seq, repeat_seq, rc, cntxt_seq=cntxt_seq)
#         row.append(a*100)
#         row.append(b*100)
#     res.append(row)
#     print(res)
# ppr.save_2d_csv(res, 'repeat_meth_pctgs.csv')

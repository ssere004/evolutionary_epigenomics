import preprocess as ppr
import process as pr
import configs
import data_reader as dr
import numpy as np
import pandas as pd
import plotting as pl
from os import path
from sklearn.cluster import KMeans
from scipy.spatial.distance import cdist
from sklearn.ensemble import RandomForestClassifier


def get_clustering_df(cnfg, rc):
    ct, allow_pickle, context = rc['coverage_threshold'], rc['allow_pickle'], rc['context']
    methylations, sequences, ad, meth_seq, cntxt_seq = dr.get_data_batch(cnfg, ct, allow_pickle=allow_pickle)
    flac_down_avg, genes_avg, flac_up_avg, g_ids = pr.get_binned_meth_lvls(ad[ad.type == 'gene'].reset_index(drop=True), meth_seq, rc, context_seq=cntxt_seq)
    meth_fgf_avg_df = pr.clean_binned_avgs(flac_down_avg, genes_avg, flac_up_avg, g_ids)
    meth_fgf_avg = meth_fgf_avg_df.loc[:, meth_fgf_avg_df.columns != 'g_ids']
    meth_fgf_avg = ppr.normalize(meth_fgf_avg)
    km_lbls, _ = pr.gbm_clustering(meth_fgf_avg)
    return meth_fgf_avg, km_lbls, meth_fgf_avg_df['g_ids']

def report_pairwise_clusterings(cnfg1, cnfg2, rc):
    ct, allow_pickle, context = rc['coverage_threshold'], rc['allow_pickle'], rc['context']
    meth_fgf_avg1, km_lbls1, g_ids1 = get_clustering_df(cnfg1, rc)
    meth_fgf_avg2, km_lbls2, g_ids2 = get_clustering_df(cnfg2, rc)
    fn = cnfg1['organism_name'][:2] + '_' + cnfg2['organism_name'][:2] + '_' + str(context) + '_clustering.png'
    avgs1, labels1 = pr.get_avgs_n_cnts(meth_fgf_avg1, km_lbls1)
    avgs2, labels2 = pr.get_avgs_n_cnts(meth_fgf_avg2, km_lbls2)
    pl.clustering_plt_pairwise(fn, avgs1, avgs2, labels1, labels2, cnfg1['organism_name'], cnfg2['organism_name'], str(context))

    return avgs1, labels1, avgs2, labels2

def train_clustering(cnfg, rc):
    meth_fgf_avg, km_lbls, g_ids = get_clustering_df(cnfg, rc)
    clf = RandomForestClassifier(n_estimators=50)
    clf.fit(meth_fgf_avg, km_lbls)
    return meth_fgf_avg, km_lbls, g_ids, clf

def set_cluster(cnfg, clf, rc):
    meth_fgf_avg, km_lbls, g_ids = get_clustering_df(cnfg, rc)
    km_lbls = clf.predict(meth_fgf_avg)
    return meth_fgf_avg, km_lbls, g_ids

def overall_classify(cnfg_list, rc):
    Xs = []
    Ls = []
    og_names = []
    cnfg = cnfg_list[0]
    meth_fgf_avg, km_lbls, g_ids, clf = train_clustering(cnfg, rc)
    X, l = pr.get_avgs_n_cnts(meth_fgf_avg, km_lbls)
    Xs.append(X)
    Ls.append(l)
    og_names.append(cnfg['organism_name'])
    print(cnfg['organism_name'] + ' added')
    for cnfg in cnfg_list[1:]:
        meth_fgf_avg, km_lbls, g_ids = set_cluster(cnfg, clf, rc)
        X, l = pr.get_avgs_n_cnts(meth_fgf_avg, km_lbls)
        Xs.append(X)
        Ls.append(l)
        og_names.append(cnfg['organism_name'])
        print(cnfg['organism_name'] + ' added')
    fn = 'panel_' + str(rc['context']) + '_clustering.png'
    pl.clustering_plt_panel(fn, Xs, Ls, og_names, str(rc['context']), 2, 3)

#Gets a cluster data_frame and then finds the average of a feature(like gene length or exon number) for the genes in each cluster
def get_mean_feature_clusters(ad, meth_fgf_avg_df, km_lbls, type):
    if type == 'exon_num':
        feature_df = ppr.make_exon_num_df(ad)
    elif type == 'gene_length':
        feature_df = ppr.make_exon_num_df(ad)
    df = pd.DataFrame({'g_ids': meth_fgf_avg_df['g_ids'], 'clust': km_lbls})
    return pd.merge(df, feature_df, how="left", on=["g_ids", "g_ids"]).groupby(by='clust').mean()


def correlation_with_repeats(cnfg, rc):
    covered_arr = pr.get_covered_genes(cnfg, rc)
    meth_fgf_avg, km_lbls, g_ids = get_clustering_df(cnfg, rc)
    meth_fgf_avg['clstr'] = km_lbls
    meth_fgf_avg['g_ids'] = g_ids
    res_df = pd.merge(meth_fgf_avg, covered_arr, how='left', on=['g_ids', 'g_ids'])
    avg_gl = ppr.get_avg_gene_length(cnfg, g_ids)
    res_df = res_df[['clstr', 'cv_down', 'cv_gb', 'cv_up']]
    avgs, labels = pr.get_avgs_n_cnts(meth_fgf_avg.loc[:, ~meth_fgf_avg.columns.isin(['clstr', 'g_ids'])], km_lbls)
    pl.clustering_plt(cnfg['organism_name'] + '_' + str(rc['context']) + '_clst.png', avgs, labels, cnfg['organism_name'], str(rc['context']))
    res_df = res_df.groupby(by='clstr').mean()
    res_df['cv_down'] = res_df['cv_down']*100/rc['flanking_size']
    res_df['cv_up'] = res_df['cv_up']*100/rc['flanking_size']
    res_df['cv_gb'] = res_df['cv_gb']*100/avg_gl
    return res_df


def elbow_clustering(X):
    distortions = []
    inertias = []
    mapping1 = {}
    mapping2 = {}
    K = range(1, 10)
    for k in K:
        kmeanModel = KMeans(n_clusters=k).fit(X)
        kmeanModel.fit(X)
        distortions.append(sum(np.min(cdist(X, kmeanModel.cluster_centers_,'euclidean'), axis=1)) / X.shape[0])
        inertias.append(kmeanModel.inertia_)
        mapping1[k] = sum(np.min(cdist(X, kmeanModel.cluster_centers_, 'euclidean'), axis=1)) / X.shape[0]
        mapping2[k] = kmeanModel.inertia_
    return mapping1, mapping2


def get_average_gene_repeat_colocation(cnfg, rc):
    covered_arr = pr.get_covered_genes(cnfg, rc)
    avg_gl = ppr.get_avg_gene_length(cnfg, covered_arr['g_ids'])
    covered_arr = covered_arr.loc[:, covered_arr.columns != 'g_ids']
    avg_size = [1000, avg_gl, 1000]
    return np.asarray(covered_arr.mean(axis=0)) / avg_size











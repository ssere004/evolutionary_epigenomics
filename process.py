import pandas as pd
import numpy as np
import sys
import constants as cntsts
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
import constants
import data_reader as dr
import preprocess as ppr
import configs
import compatibility

def get_binned_meth_lvls(genes_df, meth_seq,  rc, context_seq=None):
    bin_num, context, flanking_size, threshold = rc['bin_num'], rc['context'], rc['flanking_size'], rc['threshold']
    genes_df = genes_df.reset_index(drop=True)
    genes_avg = np.zeros((len(genes_df), bin_num), dtype=np.float)
    flac_up_avg = np.zeros((len(genes_df), bin_num), dtype=np.float)
    flac_down_avg = np.zeros((len(genes_df), bin_num), dtype=np.float)
    for index, row in genes_df.iterrows():
        is_template = row['strand'] == '+'
        seq_meth = meth_seq[row['chr']][row['start'] - flanking_size: row['end'] + flanking_size]
        if context != None:
            cntx_seq = context_seq[row['chr']][row['start'] - flanking_size: row['end'] + flanking_size]
        else:
            try:
                cntx_seq = np.zeros(row['end'] - row['start'] + 2 *flanking_size)
            except:
                print('gene with start bigger than end')
        flac_bin_size = int(flanking_size/bin_num)
        gene_bin_size = int((row['end'] - row['start'])/bin_num)
        if not is_template:
            seq_meth = seq_meth[::-1]
            if context != None:
                cntx_seq = cntx_seq[::-1]
        for i in range(bin_num):
            start = i * flac_bin_size
            end = (i+1) * flac_bin_size
            flac_down_avg[index][i] = get_meth_percentage(seq_meth[start:end], threshold, context=context, cntx_seq=cntx_seq[start:end])
        for i in range(bin_num):
            start = i * gene_bin_size + flanking_size
            end = (i+1) * gene_bin_size + flanking_size
            genes_avg[index][i] = get_meth_percentage(seq_meth[start:end], threshold, context=context, cntx_seq=cntx_seq[start:end])
        for i in range(bin_num):
            start = i * flac_bin_size + len(seq_meth) - flanking_size
            end = (i+1) * flac_bin_size + len(seq_meth) - flanking_size
            flac_up_avg[index][i] = get_meth_percentage(seq_meth[start:end], threshold, context=context, cntx_seq=cntx_seq[start:end])
    flac_down_avg = np.where(flac_down_avg == -1, np.nan, flac_down_avg)
    genes_avg = np.where(genes_avg == -1, np.nan, genes_avg)
    flac_up_avg = np.where(flac_up_avg == -1, np.nan, flac_up_avg)
    return flac_down_avg, genes_avg, flac_up_avg, genes_df.gene_id


#returns a 2D array for keeping the number of overlapped bps with down flac gene and up flac regions + gene_ids
def get_covered_genes(cnfg,  rc):
    ct, allow_pickle, context = rc['coverage_threshold'], rc['allow_pickle'], rc['context']
    methylations, sequences, ad, meth_seq, cntxt_seq = dr.get_data_batch(cnfg, ct, allow_pickle=allow_pickle)
    genes_df = ad[ad.type == 'gene']
    repeats = dr.read_annot(cnfg['repeat_address'])
    if cnfg['organism_name'] == configs.Cowpea_config['organism_name']:
        repeats = compatibility.cowpea_annotation_compatibility(repeats)
    if cnfg['organism_name'] == configs.Tomato_config['organism_name']:
        repeats = compatibility.tomato_annotation_compatibility(repeats, configs.Tomato_config['chromosomes'])
    repeat_seq = ppr.make_annotseq_string(cnfg['organism_name'], 'repeat', repeats, sequences, from_file=True, strand_spec=False)
    bin_num, context, flanking_size, threshold = rc['bin_num'], rc['context'], rc['flanking_size'], rc['threshold']
    genes_df = genes_df.reset_index(drop=True)
    covered_arr = np.zeros((len(genes_df), 3), dtype=np.float) #a 2D array for keeping the number of overlapped bps with down flac gene and up flac regions
    for index, row in genes_df.iterrows():
        is_template = row['strand'] == '+'
        seq = repeat_seq[row['chr']][row['start'] - flanking_size: row['end'] + flanking_size]
        g_length = row['end'] - row['start']
        if not is_template:
            seq = seq[::-1]
        covered_arr[index][0] = np.sum(seq[0:flanking_size])
        covered_arr[index][1] = np.sum(seq[flanking_size:flanking_size + g_length])
        covered_arr[index][2] = np.sum(seq[flanking_size + g_length:])
    covered_arr = pd.DataFrame(covered_arr, columns=['cv_down', 'cv_gb', 'cv_up'])
    covered_arr['g_ids'] = genes_df['gene_id']
    return covered_arr


#compares the methylation level in the regions of annot_seq and other regions
def get_averg_meth_seqbase(meth_seq, annot_seq, rc, cntxt_seq=None):
    sum = [0, 0] #Each element keeps the sum for one of the tags of annot_seq, for example being in a repeat or not.
    wg_sum = [0, 0]
    for key in meth_seq.keys():
        df = pd.DataFrame({'meth': meth_seq[key], 'annot': annot_seq[key][:, 0]})
        if rc['context'] != None:
            df['cntxt'] = cntxt_seq[key]
            df = df[df['cntxt'] == constants.ContextTypes.cntxt_nmbr[rc['context']]]
        df = df[df.meth != 0]
        df['meth'] = np.abs(df['meth'])
        df['meth'] = df['meth'].replace(constants.NON_METH_TAG, 0)
        sum[0] += len(df[(df['annot'] == 0) & df['meth'] > rc['threshold']])
        sum[1] += len(df[(df['annot'] == 1) & df['meth'] > rc['threshold']])
        wg_sum[0] += len(df[df['annot'] == 0])
        wg_sum[1] += len(df[df['annot'] == 1])
    return float(sum[0]/wg_sum[0]), float(sum[1]/wg_sum[1])

#finds the percentage of annot_seq2 which is in the annot_seq1, for example what percentage of repeats are in genes
def get_common_covered_seqbase(annot_seq1, annot_seq2):
    sum = 0
    wg_sum = 0
    for key in annot_seq1.keys():
        df = pd.DataFrame({'annot1': annot_seq1[key][:, 0], 'annot2': annot_seq2[key][:, 0]})
        sum += len(df[(df['annot1'] == 1) & (df['annot2'] == 1)])
        wg_sum += len(df[df['annot2'] == 1])
    return float(sum/wg_sum)

def get_coverage_pctg(annot_seq):
    sum = 0
    wg_sum = 0
    for key in annot_seq.keys():
        sum += len(annot_seq[key][annot_seq[key] == 1])
        wg_sum += len(annot_seq[key])
    return float(sum/wg_sum)

#Make a dataframe showing the all bins of 3 region methylation avgs plus the gene_Ids. The database has no NA
def clean_binned_avgs(flac_down_avg, genes_avg, flac_up_avg, g_ids):
    flac_down_avg,  genes_avg, flac_up_avg = fill_na_row_mean(pd.DataFrame(flac_down_avg)), fill_na_row_mean(pd.DataFrame(genes_avg)), fill_na_row_mean(pd.DataFrame(flac_up_avg))
    meth_fgf_avg1_df = pd.DataFrame(np.concatenate((flac_down_avg, genes_avg, flac_up_avg), axis=1))
    meth_fgf_avg1_df['g_ids'] = g_ids
    meth_fgf_avg1_df = meth_fgf_avg1_df.dropna()
    return meth_fgf_avg1_df


def fill_na_row_mean(df):
    #df = df.dropna(axis=0, how='all').reset_index(drop=True)
    return df.T.fillna(df.mean(axis=1)).T


def gbm_clustering(meth_bins_avg, n_clusters=5):
    mba_df = fill_na_row_mean(pd.DataFrame(meth_bins_avg))
    kmeans = KMeans(init="random", n_clusters=n_clusters, n_init=10, max_iter=300, random_state=42)
    kmeans.fit(mba_df)
    return kmeans.labels_, kmeans.cluster_centers_


def get_avgs_n_cnts(meth_fgf_avg, km_lbls):
    meth_fgf_avg['cluster_id'] = km_lbls
    df = meth_fgf_avg.groupby(by=['cluster_id']).mean()
    cnts = meth_fgf_avg['cluster_id'].value_counts()
    avgs = []
    labels = []
    for index, row in df.loc[:, df.columns != 'cluster_id'].iterrows():
        avgs.append(list(row))
        labels.append(str(index) + '(' + str(cnts[index]) + ')')
    return avgs, labels


def PCA_(meth_bins_avg):
    pca = PCA(n_components=2)
    Xt = pca.fit_transform(meth_bins_avg)
    return Xt

def get_meth_percentage(meth_stat, threshold, context=None, cntx_seq=None):
    if context != None:
        meth_stat = meth_stat[cntx_seq == cntsts.ContextTypes.cntx_str[context]]
    Cs = len(meth_stat[meth_stat > 0])
    if Cs > 0:
        return float(len(meth_stat[meth_stat > threshold])) / len(meth_stat[meth_stat > 0])
    else:
        return -1


def repeat_gene_dist(cnfg, annot_df):
    genes = annot_df[annot_df.type == 'gene']
    b = pybedtools.BedTool(cnfg['repeat_address'])
    genes = genes[genes.end > genes.start]
    dr.save_gff(genes, './dump_files/genes.gtf')
    a = pybedtools.BedTool('./dump_files/genes.gtf')
    c = a.sort().closest(b.sort(), S=True)
    c.saveas('./dump_files/repeat_dis_res.bed')
    d = pd.read_csv('./dump_files/repeat_dis_res.bed', sep='\t', header=None)
    df = d[[0,3,4,12,13, 17]]
    df = df[df[12] != -1]
    return df



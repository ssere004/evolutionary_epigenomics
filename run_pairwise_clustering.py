import clustering as clstr
import data_reader as dr
import configs as configs


cnfg_list = [configs.Arabidopsis_config,
             configs.Cowpea_config,
             configs.Rice_config,
             configs.Cucumber_config,
             configs.Tomato_config,
             ]

context_list = [None, 'CG', 'CHG', 'CHH']

check_list = []
avgs = {}
labels = {}
for cnfg1 in cnfg_list:
    for cnfg2 in cnfg_list:
        if cnfg1['organism_name'] == cnfg2['organism_name'] or cnfg1['organism_name']+cnfg2['organism_name'] in check_list:
            continue
        for cntx in context_list:
            rc = configs.run_config
            rc['context'] = cntx
            avgs1, labels1, avgs2, labels2 = clstr.report_pairwise_clusterings(cnfg1, cnfg2, rc)
            avgs[cnfg1['organism_name']+'_'+str(cntx)] = avgs1
            avgs[cnfg2['organism_name']+'_'+str(cntx)] = avgs2
            labels[cnfg1['organism_name']+'_'+str(cntx)] = labels1
            labels[cnfg2['organism_name']+'_'+str(cntx)] = labels2
            print(cnfg1['organism_name'] + ' ' + cnfg2['organism_name'] + ' ' + str(cntx) + ' is done')
        check_list.append(cnfg1['organism_name']+cnfg2['organism_name'])

fn_avgs = './dump_files/' + 'avgs.pkl'
fn_labels = './dump_files/' + 'labels.pkl'
dr.save_dic(fn_avgs, avgs)
dr.save_dic(fn_labels, labels)

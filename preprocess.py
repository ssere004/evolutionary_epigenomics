import data_reader
import configs
import pandas as pd
import numpy as np
from os import path
import constants
import compatibility

def get_riched_annot(on):
    ad = None
    if on == configs.Arabidopsis_config['organism_name']:
        ad = data_reader.read_annot(configs.Arabidopsis_config['annot_address'])
        ad['protein_id'] = ad['attributes'].str.extract(r' protein_id "([A-Za-z0-9_\./\\-]*)"', expand=False) #['CDS', 'start_codon', 'stop_codon']
        ad['gene_id'] = ad['attributes'].str.extract(r'gene_id "([A-Za-z0-9_\./\\-]*)"', expand=False)
    if on == configs.Cowpea_config['organism_name']:
        ad = data_reader.read_annot(configs.Cowpea_config['annot_address'])
        ad = compatibility.cowpea_annotation_compatibility(ad)
        ad['protein_id'] = ad['attributes'].str.extract(r'ID=([A-Za-z0-9]*)', expand=False) #['mRNA', 'five_prime_UTR', 'CDS', 'gene', 'three_prime_UTR']
        ad['gene_id'] = ad['attributes'].str.extract(r'ID=([A-Za-z0-9]*)', expand=False) #['mRNA', 'five_prime_UTR', 'CDS', 'gene', 'three_prime_UTR']
    if on == configs.Rice_config['organism_name']:
        ad = data_reader.read_annot('/home/csgrads/ssere004/Organisms/Rice/GCF_001433935.1_IRGSP-1.0_genomic.gff')
        ad['protein_id'] = ad['attributes'].str.extract(r';protein_id=([A-Za-z0-9_\./\\-]*)', expand=False) #['CDS']
        ad['gene_id'] = ad['attributes'].str.extract(r'GeneID:([A-Za-z0-9_\./\\-]*)', expand=False) #['CDS']
    if on == configs.Cucumber_config['organism_name']:
        ad = data_reader.read_annot(configs.Cucumber_config['annot_address'])
        ad['protein_id'] = ad['attributes'].str.extract(r';protein_id=([A-Za-z0-9_\./\\-]*)', expand=False) #['CDS']
        ad['gene_id'] = ad['attributes'].str.extract(r'GeneID:([A-Za-z0-9_\./\\-]*)', expand=False) #['CDS']
    if on == configs.Tomato_config['organism_name']:
        ad = data_reader.read_annot(configs.Tomato_config['annot_address'])
        ad['protein_id'] = ad['attributes'].str.extract(r';protein_id=([A-Za-z0-9_\./\\-]*)', expand=False)#['CDS']
        ad['gene_id'] = ad['attributes'].str.extract(r'GeneID:([A-Za-z0-9_\./\\-]*)', expand=False)#['CDS']
    return ad


def make_GeneID_to_clust_dic(res_df):
    res_dic = {}
    for index, row in res_df.iterrows():
        res_dic[row['g_ids']] = row['clstr']
    return res_dic

def make_df_from_dic(in_dic):
    return pd.DataFrame(list(in_dic.items()), columns=['keys', 'values'])

def make_CDS_to_GeneID_dic(ad):
    ad_CDS = ad[ad.type == 'CDS']
    CDS_gene_dic = {}
    for index, row in ad_CDS.iterrows():
        CDS_gene_dic[row['protein_id']] = row['gene_id']
    return CDS_gene_dic

def make_geneID_to_CDS_dic(ad):
    ad_CDS = ad[ad.type == 'CDS']
    gene_CDS_dic = {}
    for index, row in ad_CDS.iterrows():
        gene_CDS_dic[row['gene_id']] = set()
    for index, row in ad_CDS.iterrows():
        gene_CDS_dic[row['gene_id']].add(row['protein_id'])
    return gene_CDS_dic

def get_genes_contained_cds(ad):
    a = len(ad[(ad.type == 'gene') & ~(ad['gene_id'].isin(ad[ad.type == 'CDS']['gene_id']))]) #this is the number of genes which does not have a CDS
    b = len(ad[(ad.type == 'gene') & (ad['gene_id'].isin(ad[ad.type == 'CDS']['gene_id']))]) #this is the number of genes which have at least one CDS
    return a, b

def make_homoloug_gene_df(cnfg1, cnfg2):
    org1 = cnfg1['organism_name'][:2]
    org2 = cnfg2['organism_name'][:2]
    ad1 = data_reader.get_annotaions_df(cnfg1)
    ad2 = data_reader.get_annotaions_df(cnfg2)
    root = '/home/csgrads/ssere004/Organisms/common_genes/Orthologues/'
    fn = root+'Orthologues_'+org1 + '_prtm/'+org1+'_prtm__v__'+org2+'_prtm.tsv'
    hml_g = pd.read_table(fn)
    hml_g.columns = ['orthogroup', 'org1', 'org2']
    CDS_gene_dic1 = make_CDS_to_GeneID_dic(ad1)
    CDS_gene_dic2 = make_CDS_to_GeneID_dic(ad2)
    cnt = 0
    for index, row in hml_g.iterrows():
        cnt += len(row['org1'].split(', ')) * len(row['org2'].split(', '))
    gene_lst1 = [''] * cnt
    gene_lst2 = [''] * cnt
    idx = 0
    undef_cds1 = set()
    undef_cds2 = set()
    for index, row in hml_g.iterrows():
        for cds1 in row['org1'].split(', '):
            for cds2 in row['org2'].split(', '):
                try:
                    gene_lst1[idx] = CDS_gene_dic1[cds1]
                except KeyError:
                    undef_cds1.add(cds1)
                    gene_lst1[idx] = 'nan'
                try:
                    gene_lst2[idx] = CDS_gene_dic2[cds2]
                except KeyError:
                    undef_cds2.add(cds2)
                    gene_lst2[idx] = 'nan'
                idx += 1
    hml_df = pd.DataFrame({'gene1': gene_lst1, 'gene2': gene_lst2})
    hml_df = hml_df.drop_duplicates().reset_index(drop=True)
    hml_df.columns = ['gene1', 'gene2']
    return hml_df

def make_exon_num_df(ad):
    feature_df = ad[ad.type == 'exon'].groupby(by='gene_id').count()
    return pd.DataFrame({'g_ids': feature_df.index, 'feature': feature_df.chr}).reset_index(drop=True)


def make_gene_length_df(ad):
    sub_df = ad[ad.type == 'gene'].reset_index(drop=True)
    return pd.DataFrame({'g_ids': sub_df['gene_id'], 'feature': sub_df['end'] - sub_df['start']}).reset_index(drop=True)


def make_mrg_df(hml_df, ad1, ad2):
    mrg_df = pd.merge(hml_df, ad1[ad1.type == 'gene'], left_on='gene1', right_on='gene_id')
    mrg_df = mrg_df.rename(columns={'chr': 'chr1', 'start': 'start1', 'end': 'end1', 'strand': 'strand1'})
    mrg_df.drop(mrg_df.columns.difference(['gene1', 'gene2', 'chr1', 'start1', 'end1', 'strand1']), 1, inplace=True)
    mrg_df = pd.merge(mrg_df, ad2[ad2.type == 'gene'], left_on='gene2', right_on='gene_id')
    mrg_df = mrg_df.rename(columns={'chr': 'chr2', 'start': 'start2', 'end': 'end2', 'strand': 'strand2'})
    mrg_df.drop(mrg_df.columns.difference(['gene1', 'gene2', 'chr1', 'start1', 'end1', 'strand1', 'chr2', 'start2', 'end2', 'strand2']), 1, inplace=True)
    mrg_df = mrg_df.reset_index(drop=True)
    return mrg_df

def calc_avg_meth_levels(meth_seq1, meth_seq2, ad1, ad2, hml_df, context_seq1=None, context_seq2=None):
    mrg_df = make_mrg_df(hml_df, ad1, ad2)
    gene_avg_meths1 = np.zeros((len(mrg_df), 3))
    gene_avg_meths2 = np.zeros((len(mrg_df), 3))
    len_genes1 = np.zeros(len(mrg_df))
    len_genes2 = np.zeros(len(mrg_df))
    for index, row in mrg_df.iterrows():
        try:
            gene_avg_meths1[index] = get_avg_meth_gene(meth_seq1, row['chr1'], row['start1'], row['end1'], row['strand1'])
            gene_avg_meths2[index] = get_avg_meth_gene(meth_seq2, row['chr2'], row['start2'], row['end2'], row['strand2'])
            len_genes1[index] = row['end1'] - row['start1']
            len_genes2[index] = row['end2'] - row['start2']
        except IndexError:
            print(row)
    return pd.DataFrame({'gene1': mrg_df['gene1'],
                         'gene2': mrg_df['gene2'],
                         'avg_flnk_n1': gene_avg_meths1[:, 0],
                         'avg_flnk_n2': gene_avg_meths2[:, 0],
                         'avg_g1': gene_avg_meths1[:, 1],
                         'avg_g2': gene_avg_meths2[:, 1],
                         'avg_flnk_p1': gene_avg_meths1[:, 2],
                         'avg_flnk_p2': gene_avg_meths2[:, 2],
                         })

def get_avg_meth_gene(meth_seq, chr, start, end, strand, flank_size=800): #TODO change the flanking size
    res = np.zeros(3)
    seq_n = meth_seq[chr][max(start-flank_size, 0): start]
    seq_g = meth_seq[chr][start:end]
    seq_p = meth_seq[chr][end: min(len(meth_seq[chr]) - 1, end + flank_size)]
    if strand == '+':
        res[0] = get_avg_meth_seq(seq_n)
        res[1] = get_avg_meth_seq(seq_g)
        res[2] = get_avg_meth_seq(seq_p)
    else:
        res[2] = get_avg_meth_seq(seq_n)
        res[1] = get_avg_meth_seq(seq_g)
        res[0] = get_avg_meth_seq(seq_p)
    return res

def get_avg_meth_seq(m_seq):
    res = m_seq[np.nonzero(m_seq)]
    res = np.abs(res)
    res = np.where(res == constants.NON_METH_TAG, 0, res)
    if len(res) > 0:
        return np.mean(res)
    else:
        return -1

def make_meth_string(organism_name, methylations, sequences, from_file=False):
    fn = './dump_files/' + organism_name + '_meth_seq.pkl'
    if from_file and path.exists(fn):
        return data_reader.load_dic(fn)
    methylations['mlevel'] = methylations['meth']/(methylations['meth'] + methylations['unmeth'])
    methylations['coverage'] = methylations['meth'] + methylations['unmeth']
    methylations['mlevel'] = methylations['mlevel'].fillna(0)
    methylations.loc[(methylations.mlevel == 0), 'mlevel'] = constants.NON_METH_TAG
    methylations.loc[(methylations.strand == '-'), 'mlevel'] = -1 * methylations.mlevel
    meth_seq = {}
    count = 0
    for chr in sequences.keys():
        meths = np.zeros(len(sequences[chr]))
        meth_subset = methylations[methylations['chr'] == chr]
        meths[[meth_subset['position'] - 1]] = meth_subset['mlevel']
        meth_seq[chr] = meths
        count += 1
    data_reader.save_dic(fn, meth_seq)
    return meth_seq

def get_number_of_covered_CDSs(gene_CDS_dic, hml_g):
    cnt_covered = 0
    cnt_all = 0
    for g_id in gene_CDS_dic.keys():
         if len(gene_CDS_dic[g_id]) > 0:
             gene_set = gene_CDS_dic[g_id]
             try:
                 first_elem = list(gene_CDS_dic[g_id])[0]
                 if len(hml_g[hml_g.org1.str.contains(first_elem)]) == 0:
                     continue
                 hml_cds_set = set(hml_g[hml_g.org1.str.contains(first_elem)].iloc[0]['org1'].split(', '))
             except:
                 print(first_elem)
             if not gene_set.issubset(hml_cds_set):
                 cnt_covered += 1
             cnt_all += 1
    return cnt_covered, cnt_all

def make_context_string(organism_name, methylations, sequences, from_file=False):
    fn = './dump_files/' + organism_name + '_context_seq.pkl'
    if from_file and path.exists(fn):
        return data_reader.load_dic(fn)
    methylations['context_id'] = constants.ContextTypes.cntxt_nmbr['CG']
    methylations.loc[(methylations.context == 'CHG'), 'context_id'] = constants.ContextTypes.cntxt_nmbr['CHG']
    methylations.loc[(methylations.context == 'CHH'), 'context_id'] = constants.ContextTypes.cntxt_nmbr['CHH']
    context_seq = {}
    count = 0
    for chr in sequences.keys():
        contexts = np.zeros(len(sequences[chr]))
        meth_subset = methylations[methylations['chr'] == chr]
        contexts[[meth_subset['position'] - 1]] = meth_subset['context_id']
        context_seq[chr] = contexts
        count += 1
    data_reader.save_dic(fn, context_seq)
    return context_seq

def make_annotseq_string(organism_name, annot_tag, annot_df, sequences, from_file=False, strand_spec=True):
    fn = './dump_files/' + organism_name + '_' + annot_tag+'_annot_seqs.pkl'
    if from_file and path.exists(fn):
        return data_reader.load_dic(fn)
    annot_seqs = {}
    count = 0
    for chr in sequences.keys():
        annot_seq_p = np.zeros((len(sequences[chr]), 1), dtype='short')
        annot_seq_n = np.zeros((len(sequences[chr]), 1), dtype='short')
        annot_df_chr_subset = annot_df[annot_df['chr'] == chr]
        for index, row in annot_df_chr_subset.iterrows():
            if row['strand'] == '+' or not strand_spec:
                annot_seq_p[int(row['start'] - 1): int(row['end'] - 1)] = 1
            else:
                annot_seq_n[int(row['start'] - 1): int(row['end'] - 1)] = 1
            if count % int(len(annot_df) / 10) == 0:
                print(str(int(count * 100 / len(annot_df))) + '%')
            count += 1
        if strand_spec:
            annot_seqs[chr] = np.concatenate([annot_seq_p, annot_seq_n], axis=1)
        else:
            annot_seqs[chr] = annot_seq_p
    data_reader.save_dic(fn, annot_seqs)
    return annot_seqs

def get_avg_gene_length(cnfg, g_ids):
    ad = get_riched_annot(cnfg['organism_name'])
    if cnfg['organism_name'] == configs.Cowpea_config['organism_name']:
        ad = compatibility.cowpea_annotation_compatibility(ad)
    if cnfg['organism_name'] == configs.Tomato_config['organism_name']:
        ad = compatibility.tomato_annotation_compatibility(ad, cnfg['chromosomes'])
    ad = ad[ad.type == 'gene']
    gd = g_ids.to_frame()
    gd.columns = ['gene_id']
    ad = pd.merge(gd, ad, how='left', on=['gene_id', 'gene_id'])
    return np.sum(ad['end'] - ad['start'])/len(ad)

def convert_dic_tolist(dic):
    res = []
    for key in dic.keys():
        vals = list(dic[key])
        clusts = list(dic[key].keys())
        for i in range(len(vals)):
            res.append([key, clusts[i], vals[i]])
    return res

def get_nan_percentage(flac_down_avg, genes_avg, flac_up_avg, bin_num):
    a = np.count_nonzero(np.isnan(flac_down_avg))
    b = np.count_nonzero(np.isnan(genes_avg))
    c = np.count_nonzero(np.isnan(flac_up_avg))
    return (a + b + c)/(3 * bin_num * len(flac_down_avg))

def save_2d_csv(nd_array, fn):
    np.savetxt(fn, nd_array, delimiter="\t", fmt="%.3f")


def normalize(df_bin_meth):
    mean = df_bin_meth.stack().mean()
    std = df_bin_meth.stack().std()
    res = df_bin_meth.apply(lambda x: (x - mean) / std)
    return res

def get_weighted_average_fgf_meth(meth_fgf_avg1):
    a = meth_fgf_avg1[[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]].copy()
    a = a.dot(pd.Series([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]))
    a = a/55
    b = meth_fgf_avg1[[10, 11, 12, 13, 14, 15, 16, 17, 18, 19]].copy()
    b.columns = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    b = b.dot(pd.Series([1, 1, 1, 1, 1, 1, 1, 1, 1, 1]))
    b = b/10
    c = meth_fgf_avg1[[20, 21, 22, 23, 24, 25, 26, 27, 28, 29]].copy()
    c.columns = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    c = c.dot(pd.Series([10, 9, 8, 7, 6, 5, 4, 3, 2, 1]))
    c = c/10
    res_df = pd.DataFrame({'fu': a, 'gb': b, 'fd': c})
    return res_df



